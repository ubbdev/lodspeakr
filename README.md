# UB-lodspk

## Install

```bash
git clone https://gitlab.com/ubbdev/lodspeakr.git lodspk-marcus
# make gitignored components folder
git clone https://gitlab.com/ubbdev/marcus-components.git components # see install instructions in this repo
```


## Based on LODSPeaKr

author: Alvaro Graves (alvaro@graves.cl)

version: 20130612
